# README #

Below Steps are necessary to get you this flask app deployed in 3-tier architecture with the help of ansible playbook.

### What is this repository for? ###

*This repository has ansible playbook to deploy a 3-tier architectured a flaskapp deployment,so would help to understand the key benefits of ansible in such requirement.


### How do I get set up? ###

* Summary of set up
       I am using a simple flaskapp that basically work as webapp and give a bare minimum user login and a welcome page.
	   we are going to achieve this app running in 3-tier,using layer of nginx,uwsgi,python flask virtualenv and mongodb.
	   
* Configuration
       You required minimum 4 servers to be launched to run this app in a decent architecture.
	   reverse proxy - nginx - IP - count 1
	   App server - uwsgi+Flask - IP - count 2
	   Db server - mongodb - IP - count 1
* Dependencies
        Once you have built 4 servers,the following files needed to changed respect to your environment ip address.
		groups_var/all - substitute your IP's & customer values for defined variables
		webapp - substitute your proxy IP
		inventory - substitue your group of IP's
* How to run
        you should have ansible controller node and that could able to communicate all the server in the inventry.
		ansible-playbook -i inventory webapp


           

### Contribution guidelines ###

* how to rollback all the changes if it failed in between.
### Who do I talk to? ###

* g.sabarinath91@gmail.com